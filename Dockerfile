# ------------------------------------------------------------------------------
# Build validator with Cargo
# ------------------------------------------------------------------------------

FROM rust:latest as cargo-build

WORKDIR /usr/src/csm-validator

COPY . .

RUN cargo build --release --target=x86_64-unknown-linux-gnu

# ------------------------------------------------------------------------------
# Create final image
# ------------------------------------------------------------------------------

FROM debian:buster-slim

RUN groupadd -g 1000 -r csm-validator \
    && useradd --no-log-init -r -u 1000 -g csm-validator csm-validator \
    && apt-get update && apt-get install -y \
        ca-certificates \
        openssl \
    && rm -rf /var/lib/apt/lists/*

COPY --from=cargo-build /usr/src/csm-validator/target/x86_64-unknown-linux-gnu/release/csm-validator /usr/local/bin/csm-validator

USER csm-validator

WORKDIR /mnt

CMD ["csm-validator"]