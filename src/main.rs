mod error;
mod validator;

use jsonschema::JSONSchema;
use serde_json::{json, from_reader, Value};
use log::{debug, info, error, LevelFilter};
use simple_logger::SimpleLogger;
use clap::{App, Arg, crate_version, crate_authors};
use std::{env, process, fs};
use std::path::PathBuf;
use std::str::FromStr;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;


fn main() {
    let matches = App::new("CSM Validator")
        .version(crate_version!())
        .author(crate_authors!())
        .arg(Arg::with_name("LOG_LEVEL")
            .short("l")
            .long("log-level")
            .default_value("info")
            .possible_values(&["trace", "debug", "info", "warn", "error"])
            .help("Log level to use"))
        .arg(Arg::with_name("DIRECTORY")
            .help("The directory to look for CSM files in (default: current dir)"))
        .get_matches();

    let log_level = match matches.value_of("LOG_LEVEL") {
        Some(val) => LevelFilter::from_str(val).unwrap(),
        None => panic!("No log level is set?")
    };

    if let Err(err) = SimpleLogger::new().with_level(log_level).init() {
        panic!("Could not initialize logger: {}", err)
    };

    let working_dir = match matches.value_of("DIRECTORY") {
        Some(val) => PathBuf::from(val),
        None => match env::current_dir() {
            Ok(dir) => dir,
            Err(err) =>  {
                error!("Could not open cwd: {}", err);
                process::exit(255);
            }
        }
    };

    process::exit(match perform_validation(working_dir) {
        Ok(_) => {
            info!("There were no errors");
            0
        },
        Err(err) => {
            error!("There were errors: {}", err);
            1
        }
    });
}


fn perform_validation(working_dir: PathBuf) -> Result<(), Box<dyn Error>> {
    let working_dir = match fs::canonicalize(&working_dir) {
        Ok(dir) => dir,
        Err(err) => {
            error!("Could not read directory: {}", working_dir.display());
            return Err(err.into());
        }
    };

    info!("Reading directory: {}", working_dir.display());

    let csm_files = find_csm_files(&working_dir)?;
    if csm_files.is_empty() {
        return Err(error::ValidationError::new("No CSM files found".to_string()).into())
    }

    let mut error_occurred = false;
    for csm_file in csm_files {
        let file = File::open(&csm_file)?;
        let reader = BufReader::new(file);

        info!("Checking CSM: {}", csm_file.display());

        let csm_value: Value = match from_reader(reader) {
            Ok(val) => val,
            Err(err) => {
                error!("Could not parse CSM: {}", err);
                error_occurred = true;
                continue;
            }
        };

        let schema_url = match csm_value.as_object() {
            Some(obj) => match obj.get("$schema") {
                Some(schema) => schema.as_str(),
                None => None
            },
            None => None
        };

        if None == schema_url || schema_url.unwrap().is_empty() {
            error!("CSM is no object or has no valid \"$schema\" property");
            error_occurred = true;
            continue;
        }

        let validator = match validator::from_schema(schema_url.unwrap(), working_dir.clone()) {
            Some(val) => val,
            None => {
                error!("Invalid schema: {}", schema_url.unwrap());
                continue;
            }
        };

        let schema = json!({
            "$ref": schema_url.unwrap()
        });
        let compiled = JSONSchema::compile(&schema, None);
        let compiled = match compiled {
            Ok(schema) => schema,
            Err(error) => {
                error!("Error parsing schema in CSM: {:?}", error);
                continue;
            }
        };

        {
            // we create a sub scope here so that the borrow of csm_value will expire so that we
            // can move it later
            let result = compiled.validate(&csm_value);
            if let Err(errors) = result {
                for error in errors {
                    error!("Validation error: {}", error);
                }
                error_occurred = true;
                continue;
            }
        }

        if let Err(errors) = validator.validate(csm_value) {
            for error in errors {
                error!("Validation error: {}", error);
            }
            error_occurred = true;
            continue;
        };
    }
    return if error_occurred {
        Err(error::ValidationError::new("At least one CSM is invalid".to_string()).into())
    } else {
        Ok(())
    }
}


fn find_csm_files(working_dir: &PathBuf) -> Result<Vec<PathBuf>, std::io::Error> {
    let mut result = Vec::new();
    for entry in fs::read_dir(working_dir)? {
        let entry = entry?;
        let filename = entry.file_name();
        let filename_string = filename.to_string_lossy();

        debug!("Checking filename: {}", filename_string);

        // check that the file has the pattern: .csm[-suffix].json
        if filename_string.starts_with(".csm") && filename_string.ends_with(".json")
            && ['.', '-'].contains(&filename_string.chars().nth(4).unwrap()) {
            let path = entry.path();
            result.push(path);
        } else {
            debug!("Invalid filename, skipping")
        }
    }
    return Ok(result);
}
