use serde::Deserialize;
use serde_json::{Value, from_value};
use crate::error::ValidationError;
use std::error::Error;
use log::debug;
use std::path::PathBuf;
use url::Url;


pub trait Validator {
    fn validate(&self, csm: Value) -> Result<(), Vec<Box<dyn Error>>>;
}

struct CSM010Validator {
    working_dir: PathBuf
}

impl CSM010Validator {
    fn check_sockets(&self, sockets: Vec<CSM010Socket>) -> Result<(), Vec<Box<dyn Error>>> {
        let base_url = Url::from_directory_path(self.working_dir.clone()).expect("Could not create file from working dir");

        let mut errors: Vec<Box<dyn Error>> = Vec::new();
        for socket in sockets {
            CSM010Validator::check_socket_schema(&base_url, &mut errors, &socket.key_schema);
            CSM010Validator::check_socket_schema(&base_url, &mut errors, &socket.value_schema);
        }
        if !errors.is_empty() {
            Err(errors)
        } else {
            Ok(())
        }
    }

    fn check_socket_schema(base_url: &Url, errors: &mut Vec<Box<dyn Error>>, schema: &CSM010SocketSchema) {
        let schema_url = base_url.join(&schema.uri);

        if let Err(error) = schema_url {
            errors.push(error.into());
            return;
        }

        let schema_url = schema_url.unwrap();

        debug!("Checking socket schema url: {}", schema_url);

        match schema_url.scheme() {
            "file" => {
                match schema_url.to_file_path() {
                    Ok(path) => {
                        debug!("Checking local path: {}", path.display());
                        if !path.is_file() {
                            let message = format!("Local socket schema file does not exist: {}", path.display());
                            errors.push(ValidationError::new(message).into());
                        }
                    },
                    Err(_) => {
                        let message = format!("Invalid socket schema url: {}", schema_url);
                        errors.push(ValidationError::new(message).into());
                    }
                }
            },
            "http" | "https" => {
                if reqwest::blocking::get(schema_url.clone()).is_err() {
                    let message = format!("Could not retrieve socket schema url: {}", schema_url);
                    errors.push(ValidationError::new(message).into());
                }
            },
            _ => {
                let message = format!("Unsupported scheme for socket schema url: {}", schema_url);
                errors.push(ValidationError::new(message).into());
            }
        }
    }
}

impl Validator for CSM010Validator {
    fn validate(&self, csm: Value) -> Result<(), Vec<Box<dyn Error>>> {
        let mut errors: Vec<Box<dyn Error>> = Vec::new();

        let parsed: CSM010 = match from_value(csm) {
            Ok(val) => val,
            Err(error) => {
                errors.push(error.into());
                return Err(errors);
            }
        };

        debug!("Deserialized {:?}", parsed);

        if let Err(errs) = self.check_sockets(parsed.sockets.input) {
            for err in errs {
                errors.push(err);
            }
        }

        if let Err(errs) = self.check_sockets(parsed.sockets.output) {
            for err in errs {
                errors.push(err);
            }
        }

        if !errors.is_empty() {
            Err(errors)
        } else {
            Ok(())
        }
    }
}


#[derive(Deserialize, Debug)]
struct CSM010 {
    sockets: CSM010Sockets
}

#[derive(Deserialize, Debug)]
struct CSM010Sockets {
    input: Vec<CSM010Socket>,
    output: Vec<CSM010Socket>
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct CSM010Socket {
    name: String,
    key_schema: CSM010SocketSchema,
    value_schema: CSM010SocketSchema
}

#[derive(Deserialize, Debug)]
struct CSM010SocketSchema {
    uri: String
}

pub fn from_schema(schema: &str, working_dir: PathBuf) -> Option<Box<dyn Validator>> {
    match schema {
        "https://gitlab.com/centros.one/composer/schemas/-/raw/0.1.0/csm-schema.json" => Some(Box::new(CSM010Validator{working_dir})),
        _ => None
    }
}